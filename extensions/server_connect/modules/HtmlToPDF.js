const puppeteer = require('puppeteer');
const fs = require('fs');
const diacritics = require('../../../lib/core/diacritics');

exports.ConvertToPDF = async function (options) {
    // parsing whole options once so no need to do again for each params
    options = this.parse(options);

    //sanatize filename
    options.fileName = options.fileName.replace(/[\x00-\x1f\x7f!%&#@$*()?:,;"'<>^`|+={}\[\]\\\/]/g, '');
    if (options.replaceAccent) {
        options.fileName = diacritics.replace(options.fileName);
    }
    if (options.replaceASCII) {
        options.fileName = options.fileName.replace(/[^\x00-\x7e]/g, '');
    }
    if (options.replaceSpaces) {
        options.fileName = options.fileName.replace(/\s+/g, '_');
    }

    // pdf file path
    var filePath = __dirname + '/../../../' + options.folderName + '/' + options.fileName + '.pdf';

    var folderPath = __dirname + '/../../../' + options.folderName;
    if (!fs.existsSync(folderPath)) {
        fs.mkdirSync(folderPath);
    }

    const pdfOptions = {
        path: filePath, // pdf file path
        printBackground: true // will display background colors also
    };

    // pdf paper size and custom width height, we are taking unit as mm
    var pageSize = (options.paperSize == '') ? 'A4' : options.paperSize; // if null then paper is A4 by default
    if (pageSize == 'Custom') { // if custom then custom height weight of paper
        pdfOptions.width = options.pageWidth + 'mm';
        pdfOptions.height = options.pageHeight + 'mm';
    }
    else {
        pdfOptions.format = pageSize;
    }

    // pdf header footer template
    // NOTE: Pupperteer applies their own padding to header and footers. Style tags below remove/override that.
    if (options.headerHTML != null || options.footerHTML != null) { // checking if header or footer is not empty
        pdfOptions.displayHeaderFooter = true; // this flag will set header footer seperate on the paper

        if (options.headerHTML != null) { // if header template exist
            if (options.headerMargin != null) {
                pdfOptions.headerTemplate = '<style>#header { padding-top: ' + options.headerMargin + 'mm !important;}</style>' + options.headerHTML;
            }
            else {
                pdfOptions.headerTemplate = '<style>#header { padding: 0 !important; }</style>' + options.headerHTML; // if header margin is not specified then removing default header padding
            }
        }
        if (options.footerHTML != null) { // if footer template exist
            if (options.footerMargin != null) {
                pdfOptions.footerTemplate = '<style>#footer { padding-bottom: ' + options.footerMargin + 'mm !important;}</style>' + options.footerHTML;
            }
            else {
                pdfOptions.footerTemplate = '<style>#footer { padding-bottom: 0 !important;}</style>' + options.footerHTML;
            }
        }
    }

    // pdf page orientation, default is portrait. L => Landscape, P => Portrait
    if (options.orientation == 'L') {
        pdfOptions.landscape = true;
    }

    // pdf page custom margin, unit as mm
    if (options.bodyMarginLeft != null || options.bodyMarginRight != null || options.bodyMarginTop != null || options.bodyMarginBottom != null) {
        pdfOptions.margin = {
            left: (options.bodyMarginLeft == null ? 5 : options.bodyMarginLeft) + 'mm', // page left margin
            top: (options.bodyMarginTop == null ? 5 : options.bodyMarginTop) + 'mm',  // page top margin
            right: (options.bodyMarginRight == null ? 5 : options.bodyMarginRight) + 'mm',  // page right margin
            bottom: (options.bodyMarginBottom == null ? 5 : options.bodyMarginBottom) + 'mm' // page bottom margin
        };
    }


    const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox', '--single-process', '--no-zygote'] });
    const page = await browser.newPage(); // adding a new page


    if (options.dataSource == 'HTML') {
        // removing default padding and margin from body applied by Pupperteer
        var html = '<style>body { padding: 0 !important; margin: 0 !important}</style>' + options.bodyHTML; // main body html
        // waiting for dom to load complete html
        await page.setContent(html, {
            waitUntill: 'networkidle2'
        });
    }

    if (options.dataSource == 'URL') {
        await page.goto(options.bodyURL, {
            waitUntil: "networkidle2"
        });
    }
    // hack to allow web fonts to load correctly
    await page.screenshot();

    // generate and save pdf output
    await page.pdf(pdfOptions);
    await browser.close();

    var response = {};
    response.FilePath = '/' + options.folderName + '/' + options.fileName + '.pdf';
    response.FileName = options.fileName + '.pdf';
    return response;
}